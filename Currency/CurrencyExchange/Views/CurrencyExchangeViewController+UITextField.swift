//
//  CurrencyExchangeViewController+UITextField.swift
//  Currency
//
//  Created by Eduardo Leite on 26/12/19.
//  Copyright © 2019 eduardo. All rights reserved.
//

import UIKit

extension CurrencyExchangeViewController: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        self.activeTextField = textField
        return true
    }
    func textField(_ textField: UITextField,
                   shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == self.amountFromTextField || textField == self.amountToTextField {
            guard let text = textField.text else { return true }
            return currencyExchangeVM.shouldUseCharInAmount(string: string,
                                                            range: range, text: text)
        } else {
            return true
        }
    }
}
