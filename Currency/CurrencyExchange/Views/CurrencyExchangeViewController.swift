//
//  CurrencyExchangeViewController.swift
//  Currency
//
//  Created by Eduardo Leite on 25/12/19.
//  Copyright © 2019 eduardo. All rights reserved.
//

import UIKit
import SkeletonView

class CurrencyExchangeViewController: UIViewController {
    
    // MARK: ViewModel
    @objc var currencyExchangeVM: CurrencyExchangeViewModel = CurrencyExchangeViewModel()
    
    // MARK: Variables and Observers
    var activeTextField = UITextField()
    var loadingObservation: NSKeyValueObservation?
    var errorObservation: NSKeyValueObservation?
    var updateDateObservation: NSKeyValueObservation?

    // MARK: Outlets
    @IBOutlet weak var currencyFromTextField: UITextField!
    @IBOutlet weak var currencyToTextField: UITextField!
    @IBOutlet weak var amountFromTextField: UITextField!
    @IBOutlet weak var amountToTextField: UITextField!
    @IBOutlet weak var updateDateLabel: UILabel!
    @IBOutlet weak var updateBtn: UIButton!
    
    // MARK: Actions
    @IBAction func updateCurrency(_ sender: Any) {
        self.currencyFromTextField.placeholder = ""
        self.currencyToTextField.placeholder = ""
        self.amountFromTextField.placeholder = ""
        self.amountToTextField.placeholder = ""
        view.startSkeletonAnimation()
        view.showAnimatedGradientSkeleton()
        currencyExchangeVM.loadBaseExchangeRate()
    }
    
    // MARK: Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        settupBtn()
        currencyExchangeVM.loadBaseExchangeRate()
        settupTextField()
        settupObservers()
        view.showAnimatedGradientSkeleton()
    }
    
    // MARK: Settup
    func settupBtn() {
        updateBtn.backgroundColor = .clear
        updateBtn.layer.cornerRadius = 10
        updateBtn.layer.borderWidth = 1
        updateBtn.layer.borderColor = CGColor(srgbRed: 25/255, green: 118/255, blue: 210/255, alpha: 0.85)
    }
    
    func settupObservers() {
        loadingObservation = observe(\.currencyExchangeVM.loadingData, options: [.old, .new]) { _, change in
            if let loading = change.newValue {
                if !loading {
                    self.view.stopSkeletonAnimation()
                    self.view.hideSkeleton()
                    self.currencyFromTextField.placeholder = "Currency"
                    self.currencyToTextField.placeholder = "Currency"
                    self.amountFromTextField.placeholder = "Amount"
                    self.amountToTextField.placeholder = "Amount"
                }
            }
        }
        errorObservation = observe(\.currencyExchangeVM.errorString,
                            options: [.old, .new]) { _, change in
            if let errorMsg = change.newValue {
                if errorMsg != "" {
                    let alert = UIAlertController(title: "Error", message: errorMsg, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: NSLocalizedString("OK",
                                                comment: "Default action"),
                                                  style: .default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
            }
        }
        updateDateObservation = observe(\.currencyExchangeVM.currencyDateText,
                              options: [.old, .new]) { _, change in
            if let text = change.newValue {
                self.updateDateLabel.text = text
            }
        }
    }
    
    func settupTextField() {
        currencyFromTextField.delegate = self
        currencyToTextField.delegate = self
        amountFromTextField.delegate = self
        amountToTextField.delegate = self
        let thePicker = UIPickerView()
        thePicker.delegate = self
        currencyFromTextField.inputView = thePicker
        currencyToTextField.inputView = thePicker
        amountFromTextField.addTarget(self, action: #selector(self.textFieldDidChange(_:)),
                                      for: UIControl.Event.editingChanged)
        amountToTextField.addTarget(self, action: #selector(self.textFieldDidChange(_:)),
                                    for: UIControl.Event.editingChanged)
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        if (textField == self.amountFromTextField) {
            guard let amountText = amountFromTextField.text else { return }
            guard let currencyFromText = currencyFromTextField.text else { return }
            guard let currencyToText = currencyToTextField.text else { return }
            amountToTextField.text = currencyExchangeVM.converter(amount: amountText,
                                                                  from: currencyFromText, to: currencyToText)
        } else if (textField == self.amountToTextField) {
            guard let amountText = amountFromTextField.text else { return }
            guard let currencyFromText = currencyFromTextField.text else { return }
            guard let currencyToText = currencyToTextField.text else { return }
            amountFromTextField.text = currencyExchangeVM.converter(amount: amountText,
            from: currencyToText, to: currencyFromText)
        }
    }
}
