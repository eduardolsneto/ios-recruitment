//
//  CurrencyExchangeViewController+UIPickerView.swift
//  Currency
//
//  Created by Eduardo Leite on 26/12/19.
//  Copyright © 2019 eduardo. All rights reserved.
//

import UIKit

extension CurrencyExchangeViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return currencyExchangeVM.getRatesCount()
    }
    
    func pickerView( _ pickerView: UIPickerView,
                     titleForRow row: Int, forComponent component: Int) -> String? {
        return currencyExchangeVM.getRatesLabel(byIndex: row)
    }
     
    func pickerView( _ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if activeTextField == currencyToTextField {
            currencyToTextField.text = currencyExchangeVM.getRatesLabel(byIndex: row)
        } else {
            currencyFromTextField.text = currencyExchangeVM.getRatesLabel(byIndex: row)
        }
    }
}
