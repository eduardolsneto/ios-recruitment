//
//  CurrencyExchangeViewModel.swift
//  Currency
//
//  Created by Eduardo Leite on 25/12/19.
//  Copyright © 2019 eduardo. All rights reserved.
//

import Foundation

class CurrencyExchangeViewModel: NSObject {
    private var currency: CurrencyModel = CurrencyModel()
    @objc dynamic var loadingData: Bool = true
    @objc dynamic var errorString: String = ""
    @objc dynamic var currencyDateText: String = ""
    
    func createLabelsVector () {
        if let rates = currency.rates {
            currency.ratesLabels = Array(rates.keys)
        }
    }
    
    func getRatesLabel(byIndex index: Int) -> String {
        guard let ratesLabels = currency.ratesLabels else { return "" }
        return ratesLabels[index]
    }
    func getRatesCount() -> Int {
        guard let count = currency.rates?.count else { return 0 }
        return count
    }
    
    /* Note: I've noticed that it is not needed to make a request for each convertion because with we can use any base
        to convert from A to B, exemple: if a have BRL to convert to USD, and an arbitrary base A, if a divide the rate for USD in A by the rate for BRL in A and multiply by the amount (amount * A.rates["USD"]/ A.rates["BRL"]), it will be correct, so i decided to use EUR as base.
     */
    func converter(amount: String, from: String, to: String) -> String {
        guard let value = Float(amount) else { return "" }
        guard let rates = currency.rates else { return "" }
        guard let fromRate = rates[from] else { return "" }
        guard let toRate = rates[to] else { return "" }
        
        print((toRate/fromRate) * value)
        return ((toRate/fromRate) * value).description
    }
    
    func loadBaseExchangeRate() {
        APIService.getBaseExchangeRate(onComplete: { (response) in
            do {
                let data = try JSONSerialization.data(withJSONObject: response, options: .prettyPrinted)
                self.currency = try JSONDecoder().decode(CurrencyModel.self, from: data)
                self.createLabelsVector()
                self.loadingData = false
                if let date = self.currency.date {
                    self.currencyDateText = "This currency is from: " + date
                }
                let formatter = DateFormatter()
                formatter.dateFormat = preferedFormat
                let now = Date()
                let dateString = formatter.string(from:now)
                self.currencyDateText += "\n Last time Fetched: " + dateString
            } catch {
                self.errorString = fetchErrorMsg
            }
        }, onError: { (_) in
            self.errorString = internetProblemMsg
        })
    }
    func shouldUseCharInAmount(string: String, range: NSRange, text:String) -> Bool {
        let newString = (text as NSString).replacingCharacters(in: range, with: string)
        if newString.isEmpty { return true }
        if Float(newString) != nil {
            return true
        } else {
            return false
        }
    }
}
