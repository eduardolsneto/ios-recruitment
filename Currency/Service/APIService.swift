//
//  APIService.swift
//  Currency
//
//  Created by Eduardo Leite on 25/12/19.
//  Copyright © 2019 eduardo. All rights reserved.
//

import Foundation
import Alamofire

class APIService {
    class func getBaseExchangeRate(onComplete: @escaping ([String: Any]) -> Void,
                                   onError: @escaping (Error) -> Void) {
        AF.request(baseAPI + "latest").responseJSON { response in
            if let error = response.error {
                onError(error)
            } else {
                if let value = response.value as? [String: Any] {
                    onComplete(value)
                }
            }
        }
    }
}
