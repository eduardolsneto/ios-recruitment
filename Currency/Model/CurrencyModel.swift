//
//  CurrencyModel.swift
//  Currency
//
//  Created by Eduardo Leite on 25/12/19.
//  Copyright © 2019 eduardo. All rights reserved.
//

import Foundation

struct CurrencyModel: Decodable {
    var rates: [String: Float]?
    var ratesLabels: [String]?
    var base: String?
    var date: String?
}
