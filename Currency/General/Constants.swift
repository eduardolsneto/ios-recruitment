//
//  Constants.swift
//  Currency
//
//  Created by Eduardo Leite on 25/12/19.
//  Copyright © 2019 eduardo. All rights reserved.
//

import Foundation

let baseAPI = "https://api.exchangeratesapi.io/"
let internetProblemMsg = "Could not connect to the internet"
let fetchErrorMsg = "Something went wrong, please try again"
let preferedFormat = "yyyy-MM-dd HH:mm:ss"
